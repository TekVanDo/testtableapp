'use strict';

/**
 * @ngdoc function
 * @name testStaffApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the testStaffApp
 */
angular.module('testStaffApp')
    .controller('mainController', ["$resource", function ($resource) {
        var main = this,
            _staffResource = $resource('/staff/:staffId', {staffId: '@id'});

        main.selected = "";
        main.keys = [];

        main.init = function () {
            _staffResource.query(function (staff) {
                main.staff = staff;
                if(staff.length > 0) {
                    main.keys = Object.keys(staff[0]);
                    main.selected = main.keys[1];
                }
            });
        };
        main.init();
    }]);
