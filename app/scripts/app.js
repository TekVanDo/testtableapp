'use strict';

/**
 * @ngdoc overview
 * @name testStaffApp
 * @description
 * # testStaffApp
 *
 * Main module of the application.
 */
angular
  .module('testStaffApp', [
    'ngResource',
    'ngMockE2E'
  ]).run(["$httpBackend",function($httpBackend) {
      var staff = [
          {
              Position:"Programmer",
              Name:"Alexander Ivanov",
              Age:"65",
              Sex:"Male",
              Project:"SmartProject"
          },
          {
              Position:"Tester",
              Name:"Valentina Petrov",
              Age:"19",
              Sex:"Female",
              Project:"SmartProject"
          },
          {
              Position:"Programmer",
              Name:"Stepan Sidarov",
              Age:"45",
              Sex:"Male",
              Project:"SmartProject"
          },
          {
              Position:"Programmer",
              Name:"Ivan Petrov",
              Age:"20",
              Sex:"Female",
              Project:"SmartProject"
          }
      ];
      $httpBackend.whenGET('/staff').respond(staff);
      $httpBackend.whenGET('views/main.html').passThrough();
  }]);
