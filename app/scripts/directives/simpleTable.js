'use strict';

/**
 * @ngdoc directive
 * @name testTableAppApp.directive:simpleTableDirective
 * @description
 * # simpleTableDirective
 */
angular.module('testStaffApp')
  .directive('simpleTable', function () {
    return {
      scope:{
        "data":"=",
        "keys":"="
      },
      replace: true,
      template: '<table class="table table-striped table-bordered">' +
                  '<tbody>'+
                      '<tr>' +
                        '<th ng-repeat="key in keys">{{key}}</th>' +
                      '</tr>' +
                      '<tr table-row ng-repeat="one in table.data" row-object="one" keys="keys"></tr>' +
                  '</tbody>'+
                '</table>',
      restrict: 'E',
      controllerAs: 'table',
      controller: function ($scope) {
        this.data = $scope.data;
      }
    };
  });
