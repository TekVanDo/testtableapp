'use strict';

describe('Service: staffServiece', function () {

  // load the service's module
  beforeEach(module('testStaffApp'));

  // instantiate service
  var staffServiece;
  beforeEach(inject(function (_staffServiece_) {
    staffServiece = _staffServiece_;
  }));

  it('should do something', function () {
    expect(!!staffServiece).toBe(true);
  });

});
